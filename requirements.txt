Django~=3.2.12
djangorestframework==3.13.1
django-cors-headers==3.10.1
djoser==2.1.0
django-rest-auth==0.9.5