from mailbox import Mailbox
from django.contrib import admin
from  .models import Cook, Dish, Ingredients
# Register your models here.
admin.site.register(Cook)
admin.site.register(Dish)
admin.site.register(Ingredients)