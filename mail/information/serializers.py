from rest_framework import serializers
from .models import Dish, Cook, Ingredients

class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dish
        fields = ('id', 'title', 'time', 'cooker')

class CookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cook
        fields = ('id', 'fio', 'birthday', 'age', 'phone')

class IngredientsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredients
        fields = ('id', 'title', 'exp_date_days', 'dish')
