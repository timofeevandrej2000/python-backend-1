from django.shortcuts import render


from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import *
from .serializers import *

from rest_framework import viewsets
from django.shortcuts import get_object_or_404

# Create your views here.
class CookViewSet(viewsets.ModelViewSet):
        serializer_class = CookSerializer
        queryset = Cook.objects.all()

class DishViewSet(viewsets.ModelViewSet):
        serializer_class = DishSerializer
        queryset = Dish.objects.all()

class IngredientshViewSet(viewsets.ModelViewSet):
        serializer_class = IngredientsSerializer
        queryset = Ingredients.objects.all()
